image: 
  name: registry.gitlab.com/segfault-labs/docker/terraform:1.1.9
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

stages:
  - validate
  - plan
  - cost
  - apply
  - setup

cache:
  key: "$CI_COMMIT_SHA"
  paths:
    - terraform/.terraform
    - ansible/roles

.gitlab-tf-backend: &gitlab-tf-backend
  - shopt -s expand_aliases
  - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
  - export TF_ADDRESS=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_ENVIRONMENT_NAME}
  - export TF_HTTP_ADDRESS=${TF_ADDRESS}
  - export TF_HTTP_LOCK_ADDRESS=${TF_ADDRESS}/lock
  - export TF_HTTP_LOCK_METHOD=POST
  - export TF_HTTP_UNLOCK_ADDRESS=${TF_ADDRESS}/lock
  - export TF_HTTP_UNLOCK_METHOD=DELETE
  - export TF_HTTP_USERNAME=gitlab-ci-token
  - export TF_HTTP_PASSWORD=${CI_JOB_TOKEN}
  - export TF_HTTP_RETRY_WAIT_MIN=5
  - echo "Using HTTP Backend at $TF_HTTP_ADDRESS"
  - export TF_ROOT=${CI_PROJECT_DIR}/terraform
  - cd $TF_ROOT
  - terraform --version
  - terraform init -reconfigure -get=true

before_script:
  - *gitlab-tf-backend

tflint:
  stage: validate
  script:
    - tflint --init
    - tflint
  allow_failure: true
  only:
    - branches

tfsec:
  stage: validate
  script:
    - tfsec --no-color --tfvars-file envs/installfest.tfvars --format junit --out tfsec.xml .
  allow_failure: true
  artifacts:
    when: always
    paths:
      - terraform/tfsec.xml
    reports:
      junit: terraform/tfsec.xml
  only:
    - branches

validate:
  stage: validate
  script:
    - terraform validate
    - terraform fmt -check=true
  only:
    - branches

.plan:
  stage: plan
  script:
    - terraform plan -var-file=envs/${CI_ENVIRONMENT_NAME}.tfvars -out=plan.cache
    - terraform show -json plan.cache | convert_report > report-plan.json
    - terraform show -json plan.cache > full-plan.json
    - openssl aes-256-cbc -e -a -md sha512 -pbkdf2 -iter 100000 -salt -in plan.cache -out plan.cache.enc -k "${ARTIFACTS_PASSWORD}"
    - openssl aes-256-cbc -e -a -md sha512 -pbkdf2 -iter 100000 -salt -in full-plan.json -out full-plan.json.enc -k "${ARTIFACTS_PASSWORD}"
  only:
    - branches
  artifacts:
    paths:
      - terraform/full-plan.json.enc
      - terraform/report-plan.json
      - terraform/plan.cache.enc
    reports:
      terraform: terraform/report-plan.json
  resource_group: plan-and-apply

.infracost:
  stage: cost
  script:
    - openssl aes-256-cbc -d -a -md sha512 -pbkdf2 -iter 100000 -in plan.cache.enc -out plan.cache -k "${ARTIFACTS_PASSWORD}"
    - openssl aes-256-cbc -d -a -md sha512 -pbkdf2 -iter 100000 -in full-plan.json.enc -out full-plan.json -k "${ARTIFACTS_PASSWORD}"
    - >-
      infracost breakdown --path full-plan.json --format json
      --out-file infracost.json
    - infracost breakdown --path full-plan.json --format table
    - >-
      infracost comment gitlab --path infracost.json --repo $CI_PROJECT_PATH
      --commit $CI_COMMIT_SHORT_SHA --gitlab-server-url $CI_SERVER_URL --gitlab-token
      $GITLAB_TOKEN --behavior update --tag "$CI_JOB_NAME"
  artifacts:
    paths:
      - infracost.json
  only:
    - branches
  when: manual

.apply:
  stage: apply
  script:
    - openssl aes-256-cbc -d -a -md sha512 -pbkdf2 -iter 100000 -in plan.cache.enc -out plan.cache -k "${ARTIFACTS_PASSWORD}"
    - openssl aes-256-cbc -d -a -md sha512 -pbkdf2 -iter 100000 -in full-plan.json.enc -out full-plan.json -k "${ARTIFACTS_PASSWORD}"
    - terraform apply -var-file=envs/${CI_ENVIRONMENT_NAME}.tfvars -auto-approve
  only:
    - main
  when: manual
  resource_group: plan-and-apply

installfest:plan:
  extends: .plan
  environment:
    name: installfest
    action: prepare

installfest:infracost:
  extends: .infracost
  dependencies:
    - installfest:plan
  needs:
    - installfest:plan

installfest:apply:
  extends: .apply
  dependencies:
    - installfest:plan
  needs:
    - installfest:plan
  environment:
    name: installfest

.run_playbook:
  image: registry.gitlab.com/segfault-labs/docker/ansible:2.9.26
  stage: setup
  allow_failure: false
  variables:
    DEFAULT_ROLES_PATH: ${CI_PROJECT_DIR}/ansible/roles
  tags:
    - docker
  before_script:
    - chmod 700 ${CI_PROJECT_DIR}/ansible
  script:
    - cd ${CI_PROJECT_DIR}/ansible
    - ansible-galaxy install -r requirements.yml --roles-path ${DEFAULT_ROLES_PATH}
    - ./scripts/run_playbook.sh ${inventory} ${playbook}
  interruptible: false
  when: manual
  only:
    - main

installfest:setup:
  extends: .run_playbook
  variables:
    inventory: hosts
    playbook: vault-installfest
  resource_group: ansible-run
